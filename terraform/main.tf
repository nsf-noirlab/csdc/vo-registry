provider "google" {

  project = var.google_project_id
  region  = var.region
}

resource "google_project_service" "apis" {
  for_each           = toset(["artifactregistry", "run", "iamcredentials", "cloudresourcemanager", "serviceusage", "iam"])
  service            = "${each.key}.googleapis.com"
  disable_on_destroy = false
}

resource "google_service_account" "sa" {
  account_id   = "vo-registry"
  display_name = "VO registry"
}

resource "google_project_iam_member" "sa_editor" {
  project = var.google_project_id
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "run_admin" {
  project = var.google_project_id
  role    = "roles/run.admin"
  member  = "serviceAccount:${google_service_account.sa.email}"
}

# https://about.gitlab.com/blog/2023/06/28/introduction-of-oidc-modules-for-integration-between-google-cloud-and-gitlab-ci/
module "gl_oidc" {
  source                 = "gitlab.com/gitlab-com/gcp-oidc/google"
  version                = "3.2.0"
  depends_on             = [google_project_service.apis, google_project_iam_member.sa_editor, google_project_iam_member.run_admin]
  google_project_id      = var.google_project_id
  gitlab_project_id      = var.gitlab_project_id
  workload_identity_name = var.workload_identity_name
  oidc_service_account = {
    "sa" = {
      sa_email  = google_service_account.sa.email
      attribute = "attribute.project_id/${var.gitlab_project_id}"
    }
  }
}

resource "google_artifact_registry_repository" "vo-registry" {
  location      = var.region
  repository_id = "vo-registry"
  description   = "repo for registry images"
  format        = "DOCKER"
  depends_on    = [google_project_service.apis["artifactregistry"]]
  cleanup_policies {
    id     = "delete-untagged"
    action = "DELETE"
    condition {
      tag_state = "UNTAGGED"
    }
  }
  cleanup_policies {
    id     = "keep-tagged-release"
    action = "KEEP"
    condition {
      tag_state = "TAGGED"
    }
  }
}

resource "google_cloud_run_v2_service" "dev" {
  name       = "dev-vo-registry"
  location   = var.region
  depends_on = [google_project_service.apis["run"]]
  ingress    = "INGRESS_TRAFFIC_ALL"

  # to prevent needless applies since the CI is deploy the app
  # update as the deploy image updates
  client         = "gcloud"
  client_version = "483.0.0"

  template {
    containers {
      image = "${var.region}-docker.pkg.dev/${var.google_project_id}/${google_artifact_registry_repository.vo-registry.name}/registry-dev:latest"
      resources {
        cpu_idle          = true
        startup_cpu_boost = true
        limits = {
          # Memory usage limit (per container)
          # Depending on the size of the registry's resources
          # may need to increase memory
          memory = "512Mi"
          cpu    = "1000m"
        }
      }
      env {
        name  = "BASE_URL"
        value = "https://${var.dev_domain}"
      }
    }
    scaling {
      max_instance_count = 1
    }
  }
}

resource "google_cloud_run_v2_service_iam_member" "noauth-dev" {
  location = google_cloud_run_v2_service.dev.location
  name     = google_cloud_run_v2_service.dev.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

resource "google_cloud_run_v2_service" "prod" {
  name       = "vo-registry"
  location   = var.region
  depends_on = [google_project_service.apis["run"]]
  ingress    = "INGRESS_TRAFFIC_ALL"

  client         = "gcloud"
  client_version = "483.0.0"

  template {
    containers {
      image = "${var.region}-docker.pkg.dev/${var.google_project_id}/${google_artifact_registry_repository.vo-registry.name}/registry:latest"
      resources {
        cpu_idle          = true
        startup_cpu_boost = true
        limits = {
          # Memory usage limit (per container)
          # Depending on the size of the registry's resources
          # may need to increase memory
          memory = "512Mi"
          cpu    = "1000m"
        }
      }
      env {
        name  = "BASE_URL"
        value = "https://${var.prod_domain}"
      }
    }
    scaling {
      max_instance_count = 1
    }
  }
}

resource "google_cloud_run_v2_service_iam_member" "noauth-prod" {
  location = google_cloud_run_v2_service.prod.location
  name     = google_cloud_run_v2_service.prod.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

# Have to set the service account as 'owner' of the subdomain first
# https://stackoverflow.com/questions/62144372/google-cloud-api-cant-create-domain-mapping-with-app-engine-service-account
resource "google_cloud_run_domain_mapping" "dev" {
  name     = var.dev_domain
  location = google_cloud_run_v2_service.dev.location
  metadata {
    namespace = var.google_project_id
  }
  spec {
    route_name = google_cloud_run_v2_service.dev.name
  }
}

resource "google_cloud_run_domain_mapping" "prod" {
  name     = var.prod_domain
  location = google_cloud_run_v2_service.prod.location
  metadata {
    namespace = var.google_project_id
  }
  spec {
    route_name = google_cloud_run_v2_service.prod.name
  }
}
