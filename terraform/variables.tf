variable "region" {
  default = "us-west1"
}

variable "dev_domain" {
  default = "dev-template.vo.noirlab.edu"
}

variable "prod_domain" {
  default = "template.vo.noirlab.edu"
}

variable "google_project_id" {
  default = "insert_google_project_id"
}

variable "gitlab_project_id" {
  default = 9999
}

variable "workload_identity_name" {
  default = "vo-registry"
}
