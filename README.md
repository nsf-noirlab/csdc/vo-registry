# VO Publishing Registry

This repository provides the means to configure and run a VO publishing registry via Python or Docker.
VO publishing registries are used to provide metadata for compatible VO resources to other registries.

This publishing registry can be indexed by various fully searchable registries that are registered with the [IVOA Registry of Registries](http://rofr.ivoa.net/).

## QuickStart

1. Fork the repository
2. Create a branch that will contain your changes e.g. "noirlab"
3. Set the branch to be the default, mark it as protected and set it as the target for merge requests
4. [optional] Update the terraform configuration as applicable
5. Configure the Registry application  
  a. Update `pyproject.toml`
6. Deploy the application (with Terraform, Docker or Python)  
  a. **Note:** Various Gitlab pipelines are pre-defined in `.gitlab-ci.yml`

## Project Structure

### Application

The main source code for the Python application is defined in the `/registry` directory.
See the [README](registry/README.md) for more information about the application layer.

### Terraform

Various Terraform configuration files are provided in the `/terraform` directory. These are pre-configured to work with the Google Cloud Platform and usage is completely optional.
See the [README](terraform/README.md) for more information.

## Troubleshooting

### `terraform_plan` GitLab CI job failing

If you're seeing `Invalid value for \"audience\"`, the CI variables probably
aren't set and available. You can add a `- echo $WI_POOL_PROVIDER` to the
`terraform plan` CI job, if it prints `$WI_POOL_PROVIDER` instead of
`[masked]` or the value, then you either have a spelling error or you might
need to check "Expand variable reference" in the CI Variable window.
